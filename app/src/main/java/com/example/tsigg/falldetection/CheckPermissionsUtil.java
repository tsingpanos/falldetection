package com.example.tsigg.falldetection;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by tsigg on 11/7/2017.
 */

public class CheckPermissionsUtil {

    // Storage Permissions
    public static final int REQUEST_EXTERNAL_STORAGE = 0;
    public static String[] PERMISSIONS_EXTERNAL_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    public static final int REQUEST_CONTACTS = 1;
    public static String[] PERMISSIONS_CONTACTS = {
            Manifest.permission.READ_CONTACTS
    };
    public static final int REQUEST_SMS = 2;
    public static String[] PERMISSIONS_SMS = {
            Manifest.permission.SEND_SMS
    };
    public static final int REQUEST_LOCATION = 3;
    public static String[] PERMISSIONS_LOCATION = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    public static void requestStoragePermission(Activity activity) {
        int permission = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_EXTERNAL_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public static void requestContactsPermission(Activity activity) {
        int permission = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_CONTACTS,
                    REQUEST_CONTACTS
            );
        }
    }

    public static void requestSmsPermission(Activity activity) {
        int permission = ContextCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_SMS,
                    REQUEST_SMS
            );
        }
    }

    public static void requestLocationPermission(Activity activity) {
        int permission = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_LOCATION,
                    REQUEST_LOCATION
            );
        }
    }

}
