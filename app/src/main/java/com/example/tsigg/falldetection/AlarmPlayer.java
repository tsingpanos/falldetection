package com.example.tsigg.falldetection;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;

import com.example.tsigg.falldetectionlib.LibUtility;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by tsigg on 21/10/2016.
 */

public class AlarmPlayer {

    private static final String TAG = AlarmPlayer.class.getSimpleName();
    private Context mContext;
    private static MediaPlayer mMediaPlayer;
    private Uri mAlarmUri;
    private int mAlarmDuration;
    private Logger mLogger;

    private AudioManager mAudioManager;
    int mUserVolume;

    private Runnable mStopPlayerTask = new Runnable(){
        @Override
        public void run() {

            stop();
        }
    };

    public AlarmPlayer(Context context) {
        mContext = context.getApplicationContext();
        String alarm = Utility.getPrefNotificationAlarmSound(mContext);
        mAlarmUri = Uri.parse(alarm);
        mAlarmDuration = Integer.parseInt(
                Utility.getPrefNotificationAlarmDuration(mContext)
        );
        mLogger = LibUtility.getFallDetectionLogger(context);
    }

    public void play()
    {
        if (mMediaPlayer != null)
                return;
        initMediaPlayer();
        mMediaPlayer.setVolume(1.0f, 1.0f);
        mMediaPlayer.start();
        //Log.d(TAG, "Alarm started");
        mLogger.fine("Alarm started");

        Handler handler = new Handler();
        handler.postDelayed(mStopPlayerTask, mAlarmDuration*1000);
    }

    public void stop() {
        if (mMediaPlayer != null) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, mUserVolume, AudioManager.FLAG_PLAY_SOUND);
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
            //Log.d(TAG, "Alarm stopped");
            mLogger.fine("Alarm stopped");
        }
    }

    private void initMediaPlayer() {

        try {
            mMediaPlayer = new MediaPlayer();
            //mMediaPlayer.setDataSource(mContext, mAlarmUri);
            AssetFileDescriptor descriptor = mContext.getAssets().openFd("Siren.wav");
            long start = descriptor.getStartOffset();
            long end = descriptor.getLength();
            mMediaPlayer.setDataSource(descriptor.getFileDescriptor(), start, end);
            //mMediaPlayer = MediaPlayer.create(mContext, R.raw.siren);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepare();

            mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
            mUserVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_ALARM);
            mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM), AudioManager.FLAG_PLAY_SOUND);


        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
