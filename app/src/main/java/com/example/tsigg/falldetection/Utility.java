package com.example.tsigg.falldetection;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.provider.ContactsContract;

import com.example.tsigg.falldetectionlib.LibUtility;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Created by tsigg on 1/10/2016.
 */

public class Utility {
    public static final String ACTION_START = "com.example.tsigg.falldetection.action.ACTION_START_SERVICE";
    public static final String ACTION_STOP = "com.example.tsigg.falldetection.action.ACTION_STOP_SERVICE";
    public static final String ACTION_FALSE_POSITIVE_DETECTED = "com.example.tsigg.falldetection.ACTION_FALSE_POSITIVE_DETECTED";
    public static final String ACTION_FALSE_NEGATIVE_DETECTED = "com.example.tsigg.falldetection.ACTION_FALSE_NEGATIVE_DETECTED";
    public static final String TAG_CLASSIFIER = LibUtility.TAG_CLASSIFIER_KNN;
    public static final String TAG_CALIBRATION_ERROR = "com.example.tsigg.falldetection.tag.CALIBRATION_ERROR";
    public static final String FILE_MODEL_CLASSIFIER = LibUtility.FILE_MODEL_SVM;
    public static final int CALIBRATE_REQUEST_CODE = 1;
    public static final int LOCATION_UPDATE_INTERVAL = 20*60*1000; // 20mins
    public static final String LOCATION_MAP_LINK = "http://www.google.com/maps/place/%f,%f";

    public static boolean getPrefMeanFilterEnabled(Context context) {
        int filter = getPrefFilterEnabled(context);
        return filter == 2;
    }

    public static boolean getPrefMedianFilterEnabled(Context context) {
        int filter = getPrefFilterEnabled(context);
        return filter == 3;
    }

    public static boolean getPrefLowPassFilterEnabled(Context context) {
        int filter = getPrefFilterEnabled(context);
        return filter == 1;
    }

    public static boolean getPrefLowPassFilterGravityEnabled(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return prefs.getBoolean(context.getString(R.string.pref_lpfGravity_key), false);
    }

    public static int getPrefSensorFrequency(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return Integer.parseInt(
                prefs.getString(context.getString(R.string.pref_frequency_key),
                        context.getString(R.string.pref_frequency_default))
        );
    }

    public static int getPrefFilterEnabled(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return Integer.parseInt(
                prefs.getString(context.getString(R.string.pref_filter_type_key),
                context.getString(R.string.pref_filter_type_median))
        );
    }

    public static float getPrefFilterCoefficient(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return Float.parseFloat(
                prefs.getString(context.getString(R.string.pref_filter_coef_key),
                        context.getString(R.string.pref_filter_coef_default))
        );
    }

    public static float getPrefLpfGravityCoefficient(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return Float.parseFloat(
                prefs.getString(context.getString(R.string.pref_lpfGravity_coef_key),
                        context.getString(R.string.pref_lpfGravity_coef_default))
        );
    }

    public static int getPrefSubjectIdAge(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return Integer.parseInt(
                prefs.getString(context.getString(R.string.pref_subject_id_age_key),
                        context.getString(R.string.pref_subject_id_age_default))
        );
    }

    public static int getPrefSubjectIdWeight(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return Integer.parseInt(
                prefs.getString(context.getString(R.string.pref_subject_id_weight_key),
                        context.getString(R.string.pref_subject_id_weight_default))
        );
    }

    public static int getPrefSubjectIdHeight(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return Integer.parseInt(
                prefs.getString(context.getString(R.string.pref_subject_id_height_key),
                        context.getString(R.string.pref_subject_id_height_default))
        );
    }

    public static int getPrefSubjectIdGender(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        String gender = prefs.getString(context.getString(R.string.pref_subject_id_gender_key),
                context.getString(R.string.pref_subject_id_gender_male));

        return Integer.parseInt(gender);
    }

    public static String getPrefNotificationAlarmSound(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        String alarm = prefs.getString(context.getString(R.string.pref_notification_alarm_sound_key),
                context.getString(R.string.pref_notification_alarm_sound_default));

        return alarm;
    }

    public static String getPrefNotificationAlarmDuration(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        String alarm = prefs.getString(context.getString(R.string.pref_notification_alarm_duration_key),
                context.getString(R.string.pref_notification_alarm_duration_default));

        return alarm;
    }

    public static String[] getPrefNotificationContactsPhoneNumber(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        HashSet<String> contactsId = (HashSet<String>) prefs.getStringSet(context.getString(R.string.pref_notification_contacts_key), new HashSet<String>());
        List<String> phoneNos = new ArrayList<>();

        for (Iterator<String> it = contactsId.iterator(); it.hasNext();) {

            String contactId = it.next();
            Cursor pCur = context.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                    new String[]{contactId},
                    null);

            while (pCur.moveToNext()) {
                int phoneType = pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                if (phoneType == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE) {
                    String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    phoneNos.add(phoneNo);
                }
            }

        }

        return phoneNos.toArray(new String[]{});
    }

    public static String[] getPrefNotificationContactsEmail(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        HashSet<String> contactsId = (HashSet<String>) prefs.getStringSet(context.getString(R.string.pref_notification_contacts_key), new HashSet<String>());
        List<String> emails = new ArrayList<>();

        for (Iterator<String> it = contactsId.iterator(); it.hasNext();) {

            String contactId = it.next();
            Cursor pCur = context.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID +" = ?",
                    new String[]{contactId},
                    null);

            while (pCur.moveToNext()) {
                String email = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                emails.add(email);
            }

        }

        return emails.toArray(new String[]{});
    }

    public static boolean getPrefNotificationContactsTypeEmailIsEnabled(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        HashSet<String> contactsTypes = (HashSet<String>) prefs.getStringSet(context.getString(R.string.pref_notification_contactType_key), new HashSet<String>());

        for (Iterator<String> it = contactsTypes.iterator(); it.hasNext(); ) {
            String type = it.next();
            if (type.equals(context.getString(R.string.pref_notification_contactType_email)))
                return true;
        }
        return false;
    }

    public static boolean getPrefNotificationContactsTypeSmsIsEnabled(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        HashSet<String> contactsTypes = (HashSet<String>) prefs.getStringSet(context.getString(R.string.pref_notification_contactType_key), new HashSet<String>());

        for (Iterator<String> it = contactsTypes.iterator(); it.hasNext(); ) {
            String type = it.next();
            if (type.equals(context.getString(R.string.pref_notification_contactType_sms)))
                return true;
        }
        return false;
    }

    public static String[] getPrefNotificationContactsNames(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        HashSet<String> contactsTypes = (HashSet<String>) prefs.getStringSet(context.getString(R.string.pref_notification_contacts_key), new HashSet<String>());
        ArrayList<String> names = new ArrayList<>();

        for (Iterator<String> it = contactsTypes.iterator(); it.hasNext(); ) {
            String id = it.next();
            Cursor cursor = context.getContentResolver().query(
                    ContactsContract.Contacts.CONTENT_URI,
                    null,
                    ContactsContract.Contacts._ID + " = ?",
                    new String[]{id},
                    null
            );

            if (cursor.moveToFirst()) {
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                names.add(name);
            }
            cursor.close();
        }


        return names.toArray(new String[]{});
    }

    public static double[] getCalibrationError(Context context) {
        double[] error = new double[3];
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.pref_calibrate_error_key), Activity.MODE_PRIVATE);
        float x = prefs.getFloat(context.getString(R.string.pref_calibrate_xaxis_key), 0f);
        float y = prefs.getFloat(context.getString(R.string.pref_calibrate_yaxis_key), 0f);
        float z = prefs.getFloat(context.getString(R.string.pref_calibrate_zaxis_key), 0f);

        error[0] = (double) x; error[1] = (double) y; error[2] = (double) z;
        return error;
    }

    public static boolean getPrefCalibrateStartEnabled(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return prefs.getBoolean(context.getString(R.string.pref_calibrate_start_key), false);
    }

    public static boolean getPrefPowerManageEnabled(Context context) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return prefs.getBoolean(context.getString(R.string.pref_power_switch_key), false);
    }

    public static double[] getLastKnownLocation(Context context) {
        double[] location = new double[2];
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.pref_location_changed_key), Activity.MODE_PRIVATE);
        float lg = prefs.getFloat(context.getString(R.string.pref_location_long_key), 0f);
        float lt = prefs.getFloat(context.getString(R.string.pref_location_lat_key), 0f);

        location[0] = (double) lg; location[1] = (double) lt;
        return location;
    }

}
