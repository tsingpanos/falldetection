package com.example.tsigg.falldetection;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.tsigg.falldetectionlib.LibUtility;
import com.example.tsigg.falldetectionlib.classifiers.FallDetectionClassifierFactory;
import com.example.tsigg.falldetectionlib.classifiers.KnnClassifier;

import java.io.File;
import java.util.Arrays;
import java.util.logging.Logger;

//TODO: Add location.

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback{

    private static final String TAG = MainActivity.class.getSimpleName();
    private Button mBtnStart, mBtnStop;
    private FloatingActionButton mFabFall;
    private TextView mTextView, mTextViewStatus, mTextViewNotifyNone;
    private ImageView mImageViewSms, mImageViewEmail;
    private ProgressBar mProgressBar;
    private String mPath;
    private ClassifierInit mClassifierInit;
    private Logger mLogger;
    private LocationListener mLocationListener;

    private class ClassifierInit extends AsyncTask<Void, Integer, Void> {
        //MlpClassifier mlp;
        KnnClassifier knn;
        //SvmClassifier svm;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
            //mTextView.setText("App initialization may take some time...");
            mBtnStart.setEnabled(false);
            mBtnStop.setEnabled(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            publishProgress(0);
            FallDetectionClassifierFactory fallDetectionClassifierFactory = new FallDetectionClassifierFactory(getApplicationContext());
            LibUtility.assetToFile(
                    getApplicationContext(),
                    LibUtility.FILE_TRAIN_DATA,
                    LibUtility.getExternalStorageDirectory(getApplicationContext()) + File.separator + LibUtility.FOLDER_CLASSIFIER + File.separator + LibUtility.FILE_TRAIN_DATA
            );
            boolean calibrate = Utility.getPrefCalibrateStartEnabled(getApplicationContext());
            if (calibrate) {
                Intent intent = new Intent(getApplicationContext(), CalibrateActivity.class);
                startActivityForResult(intent, Utility.CALIBRATE_REQUEST_CODE);
            }
            publishProgress(50);
            //mlp = (MlpClassifier) fallDetectionClassifierFactory.getClassifier(LibUtility.TAG_CLASSIFIER_MLP);
            //publishProgress(50);
            knn = (KnnClassifier) fallDetectionClassifierFactory.getClassifier(LibUtility.TAG_CLASSIFIER_KNN);
            //publishProgress(75);
            //svm = (SvmClassifier) fallDetectionClassifierFactory.getClassifier(LibUtility.TAG_CLASSIFIER_SVM);
            publishProgress(100);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //TextView view =(TextView) findViewById(R.id.textView);
            //mTextView.setText("App initialized.");
            mProgressBar.setVisibility(View.GONE);
            mBtnStart.setEnabled(true);
            mBtnStop.setEnabled(true);
            //Log.d(TAG,"App initialized");
            mLogger.fine("App initialized");
            mLogger.fine("Calibration error: " + Arrays.toString(Utility.getCalibrationError(getApplicationContext())));
            //mLogger.fine(mlp.toString());
            mLogger.fine(knn.toString());
            //mLogger.fine(svm.toString());
            //Log.d(TAG, mlp.toString() + "\n" + knn.toString() + "\n" + svm.toString());
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            mProgressBar.setProgress(progress[0]);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkPermissions();
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                SharedPreferences locationPref = getSharedPreferences(getString(R.string.pref_location_changed_key), MODE_PRIVATE);
                SharedPreferences.Editor prefEditor = locationPref.edit();
                prefEditor.putFloat(getString(R.string.pref_location_lat_key), (float) location.getLatitude());
                prefEditor.putFloat(getString(R.string.pref_location_long_key), (float) location.getLongitude());
                prefEditor.commit();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        mPath = LibUtility.getExternalStorageDirectory(this);
        File folder = new File(mPath + File.separator + LibUtility.FOLDER_CLASSIFIER);
        if (!folder.exists()) {
            folder.mkdirs();
        }folder = new File(mPath + File.separator + LibUtility.FOLDER_LOG);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        mLogger = LibUtility.getFallDetectionLogger(getApplicationContext());

        setContentView(R.layout.activity_main);
        mTextViewStatus = (TextView) findViewById(R.id.tvStatus);
        mTextViewNotifyNone = (TextView) findViewById(R.id.tvNotifyNone);
        mImageViewEmail = (ImageView) findViewById(R.id.imageViewEmail);
        mImageViewSms = (ImageView) findViewById(R.id.imageViewSms);

        mTextView = (TextView) findViewById(R.id.tvLog);
        mFabFall = (FloatingActionButton) findViewById(R.id.fabFall);
        mFabFall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Utility.ACTION_FALSE_NEGATIVE_DETECTED);
                sendBroadcast(intent);
            }
        });

        mBtnStart = (Button) findViewById(R.id.buttonStart);
        mBtnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSensorService();
                //mFabFall.setVisibility(View.VISIBLE);
                showPauseButton();
            }
        });
        mBtnStop = (Button) findViewById(R.id.buttonStop);
        mBtnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopSensorService();
                mFabFall.setVisibility(View.GONE);
                showStartButton();
            }
        });


        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBar.setMax(100);
        mProgressBar.setVisibility(View.INVISIBLE);

        mClassifierInit = new ClassifierInit();

        if (ForegroundService.mIsRunning) {
            //mFabFall.setVisibility(View.VISIBLE);
            showPauseButton();
        }
        else {
            mFabFall.setVisibility(View.GONE);
            showStartButton();
            //mClassifierInit.execute();
        }

        showNotifications();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //checkPermissions();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Utility.LOCATION_UPDATE_INTERVAL, 0f, mLocationListener);
        }

        if (ForegroundService.mIsRunning) {
            showPauseButton();
        }
        else {
            showStartButton();
            //if (mClassifierInit.getStatus() == AsyncTask.Status.PENDING)
            //    mClassifierInit.execute();
        }

        showNotifications();

    }

    @Override
    protected void onPause() {
        super.onPause();
        //checkPermissions();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
            locationManager.removeUpdates(mLocationListener);
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, Utility.LOCATION_UPDATE_INTERVAL, 0f, mLocationListener);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if (ForegroundService.mIsRunning)
                stopSensorService();
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void stopSensorService() {
        //Log.d(TAG, "Stop Service intent sent.");
        mLogger.fine("Stop Service intent sent");
        Intent intent = new Intent(this, ForegroundService.class );
        intent.setAction(Utility.ACTION_STOP);
        startService(intent);
    }

    private void startSensorService() {
        //Log.d(TAG, "Start Service intent sent.");
        mLogger.fine("Start Service intent sent");
        Intent intent = new Intent(this, ForegroundService.class );
        intent.setAction(Utility.ACTION_START);
        startService(intent);
    }

    @Override
    protected void onDestroy() {
        //Log.d(TAG, "MainActivity Destroyed.");
        mLogger.fine("MainActivity Destroyed");
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Utility.CALIBRATE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                double error[] = data.getDoubleArrayExtra(Utility.TAG_CALIBRATION_ERROR);
                //Log.d(TAG, Arrays.toString(error));

                SharedPreferences calibrateXPref = getSharedPreferences(getString(R.string.pref_calibrate_error_key), MODE_PRIVATE);
                SharedPreferences.Editor prefEditor = calibrateXPref.edit();
                prefEditor.putFloat(getString(R.string.pref_calibrate_xaxis_key), (float) error[0]);
                prefEditor.putFloat(getString(R.string.pref_calibrate_yaxis_key), (float) error[1]);
                prefEditor.putFloat(getString(R.string.pref_calibrate_zaxis_key), (float) error[2]);
                prefEditor.commit();
            }
        }
    }

    private void checkPermissions() {
        CheckPermissionsUtil.requestContactsPermission(this);
        CheckPermissionsUtil.requestLocationPermission(this);
        CheckPermissionsUtil.requestSmsPermission(this);
        CheckPermissionsUtil.requestStoragePermission(this);
    }

    private void showStartButton() {
        mBtnStart.setVisibility(View.VISIBLE);
        mBtnStop.setVisibility(View.GONE);

        mTextViewStatus.setText(getString(R.string.txtStatusTxt) + " " + getString(R.string.txtStatusOffTxt));
    }

    private void showPauseButton() {
        mBtnStart.setVisibility(View.GONE);
        mBtnStop.setVisibility(View.VISIBLE);

        mTextViewStatus.setText(getString(R.string.txtStatusTxt) + " " + getString(R.string.txtStatusOnTxt));
    }

    private void showNotifications() {
        boolean mailEnabled = Utility.getPrefNotificationContactsTypeEmailIsEnabled(this);
        boolean smsEnabled = Utility.getPrefNotificationContactsTypeSmsIsEnabled(this);

        if (mailEnabled)
            mImageViewEmail.setVisibility(View.VISIBLE);
        else
            mImageViewEmail.setVisibility(View.GONE);

        if (smsEnabled)
            mImageViewSms.setVisibility(View.VISIBLE);
        else
            mImageViewSms.setVisibility(View.GONE);

        if (!(smsEnabled || mailEnabled))
            mTextViewNotifyNone.setVisibility(View.VISIBLE);
        else
            mTextViewNotifyNone.setVisibility(View.GONE);

    }

    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission was granted, yay! Do the
            // contacts-related task you need to do.
            if (requestCode == CheckPermissionsUtil.REQUEST_EXTERNAL_STORAGE)
                mClassifierInit.execute();

        } else {
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
            finish();
        }
        return;
    }
}
