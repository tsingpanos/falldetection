package com.example.tsigg.falldetection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.tsigg.falldetectionlib.filters.MeanFilter;

public class CalibrateActivity extends AppCompatActivity implements SensorEventListener{

    private double[] mError;
    private MeanFilter mFilter;
    private SensorManager mSensorManager;
    private Button mButton;

    private Runnable mStopCalibration = new Runnable() {
        @Override
        public void run() {
            mSensorManager.unregisterListener(CalibrateActivity.this);
            Intent data = new Intent();
            data.putExtra(Utility.TAG_CALIBRATION_ERROR, mError);
            setResult(Activity.RESULT_OK, data);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibrate);

        mFilter = new MeanFilter();
        mFilter.setTimeConstant(1);
        mError = new double[3];

        mButton = (Button) findViewById(R.id.buttonCalibrate);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calibrateSensor();
                Handler handler = new Handler();
                handler.postDelayed(mStopCalibration, 1000);
                mButton.setEnabled(false);
            }
        });

    }

    private void calibrateSensor() {
        mSensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        int freq = Utility.getPrefSensorFrequency(this);
        mSensorManager.registerListener(this, sensor, 1000000 / freq);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            double acceleration[] = new double[3];
            for (int i = 0; i < 3; i++)
                acceleration[i] = (double) event.values[i];
            acceleration = mFilter.addSamples(acceleration);
            mError[0] = 0 - acceleration[0];
            mError[1] = 0 - acceleration[1];
            mError[2] = SensorManager.GRAVITY_EARTH - acceleration[2];
        }
    }
}
