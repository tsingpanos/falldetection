package com.example.tsigg.falldetection;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;

import java.util.concurrent.ExecutionException;


public class AlarmActivity extends AppCompatActivity {

    private static final String TAG = AlarmActivity.class.getSimpleName();
    private static final String EMAIL_USER = "";
    private static final String EMAIL_PASS = "";
    private Boolean mFalsePositive = false;
    private AlertDialog mAlert;
    private double[] mLocation = new double[2];
    private final String MESSAGE = "Fall occurred. User is in danger.\nLocation: %.6f, %.6f\nGoogle Maps link: %s";

    private DonutProgress mProgressBar;
    private ImageButton mOkButton;

    private class NotifySMS extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            Context context = AlarmActivity.this.getApplicationContext();
            //String[] names = Utility.getPrefNotificationContactsNames(context);

            if (Utility.getPrefNotificationContactsTypeSmsIsEnabled(context)) {
                try {
                    //String summary = "SMS was sent to ";
                    String[] phoneNos = Utility.getPrefNotificationContactsPhoneNumber(context);
                    String link = String.format(Utility.LOCATION_MAP_LINK, mLocation[1], mLocation[0]);
                    String message = String.format(MESSAGE, mLocation[1], mLocation[0], link);
                    SmsManager smsManager = SmsManager.getDefault();
                    for (int i = 0; i < phoneNos.length; i++) {
                        //summary += (phoneNos[i] + ", ");
                        smsManager.sendTextMessage(phoneNos[i], null, message, null, null);
                    }

                    //summary = summary.replaceFirst("(,\\s)$", "");

                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }
    }

    private class NotifyEmail extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            Context context = AlarmActivity.this.getApplicationContext();

            if (Utility.getPrefNotificationContactsTypeEmailIsEnabled(context)) {
                String[] emails = Utility.getPrefNotificationContactsEmail(context);

                Mail m = new Mail(EMAIL_USER, EMAIL_PASS);
                //String[] toArr = {"bla@bla.com", "lala@lala.com"};
                m.setHost("smtp.mail.yahoo.com");
                m.setTo(emails);
                m.setFrom(EMAIL_USER);

                m.setSubject("Fall Detection");
                String link = String.format(Utility.LOCATION_MAP_LINK, mLocation[0], mLocation[1]);
                String message = String.format(MESSAGE, mLocation[0], mLocation[1], link);
                m.setBody(message);

                try {
                    if (m.send()) {
                        return true;
                    } else {
                        return false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return false;
        }
    }

    private Runnable mTimer = new Runnable() {
        @Override
        public void run() {
            //Log.d(TAG, "mTimer fired");
            if (!mFalsePositive) {

                NotifySMS notifySMS = new NotifySMS();
                notifySMS.execute();
                try {
                    if (notifySMS.get()) {
                        Toast toast = Toast.makeText(AlarmActivity.this.getApplicationContext(), "SMS was sent successfully.", Toast.LENGTH_LONG);
                        toast.show();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                NotifyEmail notifyEmail = new NotifyEmail();
                notifyEmail.execute();
                try {
                    if (notifyEmail.get()) {
                        Toast toast = Toast.makeText(AlarmActivity.this.getApplicationContext(), "Email was sent successfully.", Toast.LENGTH_LONG);
                        toast.show();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
/*
                if (mAlert.isShowing())
                    mAlert.cancel();
                    mAlert.dismiss();
*/
            }
            AlarmActivity.this.finish();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.d(TAG, "Alarm Activity started.");

        setContentView(R.layout.activity_alarm);
        mProgressBar = (DonutProgress) findViewById(R.id.progressBar2);
        mOkButton = (ImageButton) findViewById(R.id.imageButton);

        final AlarmPlayer alarm = new AlarmPlayer(this.getApplicationContext());
        alarm.play();

        final Context context = AlarmActivity.this.getApplicationContext();

        int alarmDuration = Integer.parseInt(
                Utility.getPrefNotificationAlarmDuration(context)
        );

        final CountDownTimer countDownTimer = new CountDownTimer(alarmDuration*1000, 1000) {
            @Override
            public void onTick(long l) {
                int progress = (int) l/100;
                mProgressBar.setProgress(mProgressBar.getMax()-progress);
                mProgressBar.setText((int)l/1000 + " secs");
            }

            @Override
            public void onFinish() {
                alarm.stop();
                AlarmActivity.this.finish();
                final Handler handler = new Handler();
                handler.post(mTimer);
            }
        };
        countDownTimer.start();

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.d(TAG, "User decided on FP");

                Intent intent = new Intent();
                intent.setAction(Utility.ACTION_FALSE_POSITIVE_DETECTED);
                context.sendBroadcast(intent);

                alarm.stop();
                mFalsePositive = true;
                AlarmActivity.this.finish();
                countDownTimer.cancel();
            }
        });

        mLocation = Utility.getLastKnownLocation(getApplicationContext());

/*
        final Context context = AlarmActivity.this.getApplicationContext();
        int alarmDuration = Integer.parseInt(
                Utility.getPrefNotificationAlarmDuration(context)
        );
        final AlarmPlayer alarm = new AlarmPlayer(this.getApplicationContext());
        alarm.play();

        final Handler handler = new Handler();
        handler.postDelayed(mTimer, alarmDuration*1000);

        AlertDialog.Builder builder = new AlertDialog.Builder(
                //new ContextThemeWrapper(this, R.style.AlertDialogCustom)
                this
        );
        builder
                .setTitle("Fall detected")
                .setMessage("Press the button to STOP the alarm")
                .setCancelable(false)
                .setIcon(R.drawable.ic_cancel_black_100dp)
                .setPositiveButton("STOP", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d(TAG, "User decided on FP");

                        Intent intent = new Intent();
                        intent.setAction(Utility.ACTION_FALSE_POSITIVE_DETECTED);
                        context.sendBroadcast(intent);

                        alarm.stop();
                        mFalsePositive = true;
                        dialogInterface.cancel();
                        dialogInterface.dismiss();
                        AlarmActivity.this.finish();
                    }
                });

        mAlert = builder.create();
        mAlert.show();
        mAlert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                handler.removeCallbacks(mTimer);
            }
        });

        mLocation = Utility.getLastKnownLocation(this);
*/
    }

}
