package com.example.tsigg.falldetection;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.preference.MultiSelectListPreference;
import android.provider.ContactsContract;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by tsigg on 12/11/2016.
 */

public class CustomContactsPreference extends MultiSelectListPreference {

    private static final String TAG = CustomContactsPreference.class.getSimpleName();

    private Context mContext;
    private Cursor mContactsCursor;
    private List<CharSequence> mEntries = new ArrayList<>();
    private List<CharSequence> mEntryValues = new ArrayList<>();

    public CustomContactsPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context.getApplicationContext();

        ContentResolver cr = mContext.getContentResolver();
        Cursor cur = cr.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                ContactsContract.Contacts.DISPLAY_NAME + " ASC"
        );

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {

                    mEntryValues.add(id);
                    mEntries.add(name);

                    /*
                    // Query phone here. Covered next
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[] { id },
                            null
                    );
                    while (pCur.moveToNext()) {
                        // Do something with phones
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        nameList.add(name); // Here you can list of contact.
                        phoneList.add(phoneNo); // Here you will get list of phone number.


                        Cursor emailCur = cr.query(
                                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                                new String[]{id}, null);
                        if (emailCur.moveToNext()) {
                            String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

                            emailList.add(email); // Here you will get list of email

                        }
                        emailCur.close();
                    }
                    pCur.close();
                */

                }
            }
        }

        setValues(new HashSet<String>());
        setEntries(mEntries.toArray(new CharSequence[]{}));
        setEntryValues(mEntryValues.toArray(new CharSequence[]{}));
    }

}
