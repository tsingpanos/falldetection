package com.example.tsigg.falldetection;
/*
@startuml

namespace falldetection {

    namespace falldetectionlib{
    }

    namespace app {
    }
}

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml

namespace app {
    class MainActivity
    class AlarmActivity
    class SettingsActivity
    class CalibrateActivity
    class ForegroundService
    class AlarmPlayer
    class Mail
    class MyReceiver
    class Utility
}

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml
class MainActivity extends Activity {
    -mClassifierInit : ClassifierInit
    -mStartBtn : Button
    -mStopBtn : Button
    -mProgressBar : ProgressBar
    #onCreate(Bundle)
    #onResume()
    -startService()
    -stopService()
}

class ClassifierInit extends AsyncTask {
}
class AsyncTask {
    #onPreExecute()
    #doInBackground()
    #onPostExecute()
    #onProgressUpdate(Integer)
}
MainActivity +-- ClassifierInit

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml
class AlarmActivity extends Activity {
    -mTimer : Runnable
    -mFalsePositive : Boolean
    -mAlert : AlertDialog
    -onStop() : DialogInterface.OnClickListener
    #onCreate(Bundle)
}

class NotifySMS extends AsyncTask {
}
class NotifyEmail extends AsyncTask {
}
class AsyncTask {
    #doInBackground()
}
AlarmActivity +-- NotifySMS
AlarmActivity +-- NotifyEmail

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*

@startuml

class MyReceiver extends BroadcastReceiver{
    +onReceive(Context, Intent)
}

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml

floating note: <i>onReceive</i> method flowchart
start
if (FSM_FALL) then (yes)
    :Start <i>ClassifyService</i> ]
elseif (FALL) then (yes)
    :Start <i>AlarmActivity</i> ]
elseif (FALSE_POSITIVE) then (yes)
    :Update classifier\nwith new training data]
endif

stop

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml

class ForegroundService extends Service{
    -mDataQueue : BlockingQueue
    -mListener : CustomSensorEventListener
    -mFsm : FSM
    -mWakeLock : WakeLock
    +{static}mIsRunning : Boolean
    +onCreate()
    +onStartCommand(Intent, Integer, Integer) : Integer
    +onDestroy()
    -stopService()
    -initFilter() : Filter
    -setSensorDelay(Integer)
    -registerSensor()
}
@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml

class AlarmPlayer {
    -{static}mMediaPlayer : MediaPlayer
    -mAlarmDuration : Integer
    -mStopPlayerTask : Runnable
    +AlarmPlayer(Context)
    +play()
    +stop()
    -initMediaPlayer()
}

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml

class SettingsActivity extends AppCompatPreferenceActivity {
    -{static}sBindPreferenceSummaryToValueListener : Preference.OnPreferenceChangeListener
    #onCreate(Bundle)
    -{static}bindPreferenceSummaryToValue(Preference)
}

SettingsActivity +-- SubjectIdPreferenceFragment
class SubjectIdPreferenceFragment extends PreferenceFragment
SettingsActivity +-- NotificationPreferenceFragment
class NotificationPreferenceFragment extends PreferenceFragment

class PreferenceFragment {
    +onCreate(Bundle)
    +onOptionsItemSelected(MenuItem) : Boolean
}

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml

class Utility {
    +{static}ACTION_START : String
    +{static}ACTION_STOP : String
    +{static}ACTION_FALSE_POSITIVE_DETECTED : String
    +{static}TAG_CLASSIFIER : String
    +{static}getPrefMeanFilterEnabled(Context) : Boolean
    +{static}getPrefMedianFilterEnabled(Context) : Boolean
    +{static}getPrefLowPassFilterEnabled(Context) : Boolean
    '+{static}getPrefLowPassFilterGravityEnabled(Context) : Boolean'
    +{static}getPrefSensorFrequency(Context) : Integer
    +{static}getPrefFilterEnabled(Context) : Integer
    +{static}getPrefFilterCoefficient(Context) : Float
    +{static}getPrefLpfGravityCoefficient(Context) : Float
    +{static}getPrefSubjectIdAge(Context) : Integer
    +{static}getPrefSubjectIdWeight(Context) : Integer
    +{static}getPrefSubjectIdHeight(Context) : Integer
    +{static}getPrefSubjectIdGender(Context) : Integer
    +{static}getPrefNotificationAlarmSound(Context) : String
    +{static}getPrefNotificationAlarmDuration(Context) : String
    +{static}getPrefNotificationContactsPhoneNumber(Context) : String[]
    +{static}getPrefNotificationContactsEmail(Context) : String[]
    +{static}getPrefNotificationContactsNames(Context) : String[]
    +{static}getPrefNotificationContactsTypeEmailIsEnabled(Context) : Boolean
    +{static}getPrefNotificationContactsTypeSmsIsEnabled(Context) : Boolean
}

@enduml
 */

////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml

class CalibrateActivity extends Activity implements SensorEventListener {
    -mError : Double[]
    -mButton : Button
    -mStopCalibration : Runnable
    -calibrateSensor()
}

@enduml
 */