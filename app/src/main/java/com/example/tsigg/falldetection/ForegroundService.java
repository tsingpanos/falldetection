package com.example.tsigg.falldetection;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.example.tsigg.falldetectionlib.CustomSensorEventListener;
import com.example.tsigg.falldetectionlib.LibUtility;
import com.example.tsigg.falldetectionlib.filters.Filter;
import com.example.tsigg.falldetectionlib.filters.LowPassFilter;
import com.example.tsigg.falldetectionlib.filters.MeanFilter;
import com.example.tsigg.falldetectionlib.filters.MedianFilter;
import com.example.tsigg.falldetectionlib.filters.NoFilter;
import com.example.tsigg.falldetectionlib.fsm.FSM;
import com.example.tsigg.falldetectionlib.fsm.InputFSM;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

/**
 * Created by tsigg on 7/10/2016.
 */

public class ForegroundService extends Service {
    private static final String TAG = ForegroundService.class.getSimpleName();

    // Service
    private static final int ONGOING_NOTIFICATION_ID = 1;

    // CSV
    private static final char CSV_DELIM = ',';
    private static final String CSV_HEADER = "Time,Hz,X_Axis,Y_Axis,Z_Axis";
    private static final String CSV_INFOS = "Gender: %s\nAge: %s\nWeight: %s\nHeight: %s";
    private static final String CSV_INFOA = "Accelerometer data in g\nSensor: %s\nStart time: %s\nAccelerometer filter type: %s\n";

    // Filters
    private SensorManager mSensorManager;
    private CustomSensorEventListener mListener;

    // Logger
    private String mActivityName = "DefaultActivity";
    private PrintWriter mPrintWriterAcc;
    private String mPath;
    private Logger mLogger;

    // Time
    private int mFrequencySelection;

    // Outputs for the mAcceleration and LPFs
    private volatile BlockingQueue mDataQueueFSM, mDataQueueLog;

    // FSM
    private FSM mFsm;

    // Threads
    private Thread mThreadFSM, mThreadDataLog;
    private HandlerThread mHandlerThreadSensor;

    // Power
    private PowerManager.WakeLock mWakeLock;
    private Boolean mPowerManageEnable = true;
    private double mMuM = 0, mSigmaSqM = 0;
    private int mNum = 0;
    private final int mNormalRange = 2, mActivityBreak = 5000;
    private boolean mActivityState = false, mActivityValid = false;
    private double mActivityStart = 0, mActivityEnd = 0;

    public static volatile boolean mIsRunning = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mLogger = LibUtility.getFallDetectionLogger(getApplicationContext());
        //Log.d(TAG, "Service created");
        mLogger.fine("Service created");

        mSensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        mFsm = new FSM(this);

        mHandlerThreadSensor = new HandlerThread("SensorListener");
        mHandlerThreadSensor.start();

        mPowerManageEnable = Utility.getPrefPowerManageEnabled(getApplicationContext());

        mThreadFSM = new Thread(new Runnable() {
            @Override
            public void run() {
                while (mDataQueueFSM != null)
                    try {
                        double[] data = (double[]) mDataQueueFSM.take();
                        InputFSM inputFSM = new InputFSM(data[0], data[2], data[3], data[4]);
                        mFsm.run(inputFSM);
                        if (mPowerManageEnable)
                            activation(data);
                        //String ft = mFsm.getFSMFallTransition();
                        //String wt = mFsm.getFSMWalkTransition();
                        //logFsm(ft, wt);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return;
                    }
            }
        }, "FsmThread");

        mThreadDataLog = new Thread(new Runnable() {
            @Override
            public void run() {
                while (mDataQueueLog != null)
                    try {
                        double[] data = (double[]) mDataQueueLog.take();
                        logData(data);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return;
                    }
            }
        }, "DataLogThread");

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "FallDetectionWakelockTag");

        mPath = LibUtility.getExternalStorageDirectory(this);
        File folder = new File(mPath);
        if (!folder.exists())
            folder.mkdirs();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            final String action = intent.getAction();
            if (Utility.ACTION_START.equals(action)) {
                //Log.d(TAG, "Service started");
                mLogger.fine("Service started");
                mActivityName = intent.getStringExtra("ActivityName");
                mIsRunning = true;
                loggerDataInit();

                mWakeLock.acquire();
                double[] calibrationError = Utility.getCalibrationError(this);
                //Log.d(TAG, Arrays.toString(calibrationError));
                mDataQueueFSM = new ArrayBlockingQueue(50);
                mDataQueueLog = new ArrayBlockingQueue(50);
                mThreadFSM.start();
                mThreadDataLog.start();
                Filter filter = initFilter();
                mListener = new CustomSensorEventListener(filter, calibrationError, new BlockingQueue[]{mDataQueueLog, mDataQueueFSM});
                registerSensor();

                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.ic_service_black)
                                .setContentTitle(getText(R.string.notification_title))
                                .setContentText(getText(R.string.notification_message));
                Intent notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder.setContentIntent(pendingIntent);
                startForeground(ONGOING_NOTIFICATION_ID, mBuilder.build());

            } else if (Utility.ACTION_STOP.equals(action)) {
                stopService();
                //Log.d(TAG, "Service stopped");
                mLogger.fine("Service stopped");
            }
        }

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mIsRunning)
            stopService();

        //Log.d(TAG, "Service destroyed");
        mLogger.fine("Service destroyed");
    }

    private void stopService() {
        mIsRunning = false;
        mSensorManager.unregisterListener(mListener);
        mListener.reset();
        // Quit SensorListener handler
        if (mHandlerThreadSensor.isAlive())
            mHandlerThreadSensor.quitSafely();
        // Close data writer
        if (mPrintWriterAcc != null)
            mPrintWriterAcc.close();
        // Clear queues
        if (mDataQueueFSM != null) {
            mDataQueueFSM.clear();
            mDataQueueFSM = null;
        }
        if (mDataQueueLog != null) {
            mDataQueueLog.clear();
            mDataQueueLog = null;
        }
        // Quit threads
        if (mThreadFSM != null) {
            Thread dummy = mThreadFSM;
            mThreadFSM = null;
            dummy.interrupt();
        }
        if (mThreadDataLog != null) {
            Thread dummy = mThreadDataLog;
            mThreadDataLog = null;
            dummy.interrupt();
        }
        // Release wakelock
        if (mWakeLock.isHeld())
            mWakeLock.release();
        stopForeground(true);
        stopSelf();
        //log();
    }

    private void log() {
        try {
            String date = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", Locale.US).format(new Date());
            String s = String.format("logcat -v threadtime -d -f %s", mPath + File.separator + "Log_" + mActivityName +"_" + date + ".txt");
            Process process = Runtime.getRuntime().exec(s);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loggerDataInit() {
        //Log.d(TAG, "Logger Data initialization ...");
        mLogger.fine("Logger Data initialization ...");

        int gender = Utility.getPrefSubjectIdGender(this);
        int age = Utility.getPrefSubjectIdAge(this);
        int weight = Utility.getPrefSubjectIdWeight(this);
        int height = Utility.getPrefSubjectIdHeight(this);
        String subject = String.format("%s\n%s\n%s\n%s", gender, age, weight, height);
        String date = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", Locale.US).format(new Date());
        String sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER).getName();
        String csvInfoA = String.format("Accelerometer data in g\n%s\n%s\n%s\n",
                sensor,
                date,
                Utility.getPrefFilterEnabled(this));
        //Log.d(TAG, "Subject info: " + subject.replace('\n', ','));
        mLogger.fine("Subject info: " + subject.replace('\n', ','));

        try {
            mPrintWriterAcc = new PrintWriter(new FileOutputStream(new File(mPath, "Data_" + date + ".csv")));

            mPrintWriterAcc.println(subject);
            mPrintWriterAcc.println(csvInfoA);
            mPrintWriterAcc.println(CSV_HEADER);
        } catch (IOException e) {
            //Log.e(TAG, "Could not open CSV file(s)", e);
            mLogger.severe("Could not open CSV file(s)" + e);
        }
    }

    private void logData(double[] data) {
        StringBuilder sb = new StringBuilder().append(data[0]).append(CSV_DELIM)
                .append(data[1]).append(CSV_DELIM)
                .append(data[2]).append(CSV_DELIM)
                .append(data[3]).append(CSV_DELIM)
                .append(data[4]);
        mPrintWriterAcc.println(sb.toString());
        if (mPrintWriterAcc.checkError()) {
            //Log.e(TAG, "Error writing accelerometer sensor event data");
            mLogger.severe("Error writing accelerometer sensor event data");
        }
    }

    private Filter initFilter() {
        //Log.d(TAG, "Filters initialization...");
        mLogger.fine("Filters initialization...");
        Boolean meanFilterSmoothEnabled = Utility.getPrefMeanFilterEnabled(this);
        Boolean medianFilterSmoothEnabled = Utility.getPrefMedianFilterEnabled(this);
        Boolean lowPassFilterSmoothEnabled = Utility.getPrefLowPassFilterEnabled(this);
        double filterCoefficient = Utility.getPrefFilterCoefficient(this);
        Filter filter = new NoFilter();

        /*
        Log.d(TAG, "Mean Filtered " + (mMeanFilterSmoothEnabled ? "enabled" : "disabled"));
        Log.d(TAG, "Median Filtered " + (mMedianFilterSmoothEnabled ? "enabled" : "disabled"));
        Log.d(TAG, "LPF Filtered " + (mLowPassFilterSmoothEnabled ? "enabled" : "disabled"));
        Log.d(TAG, "Filter coefficient set to " + filterCoefficient + "sec");
        */
        mLogger.fine("Mean Filtered " + (meanFilterSmoothEnabled ? "enabled" : "disabled"));
        mLogger.fine("Median Filtered " + (medianFilterSmoothEnabled ? "enabled" : "disabled"));
        mLogger.fine("LPF Filtered " + (lowPassFilterSmoothEnabled ? "enabled" : "disabled"));
        mLogger.fine("Filter coefficient set to " + filterCoefficient + "sec");

        if (meanFilterSmoothEnabled) {
            filter = new MeanFilter();
            filter.setTimeConstant(filterCoefficient);       // 0.06f
        }

        if (medianFilterSmoothEnabled) {
            filter = new MedianFilter();
            filter.setTimeConstant(filterCoefficient);       // 0.06f
        }

        if (lowPassFilterSmoothEnabled) {
            filter = new LowPassFilter();
            filter.setTimeConstant(filterCoefficient);       // 0.02f
        }

        return filter;
    }


    private void setSensorDelay(int frequency) {

        int delay = 1000000 / frequency;

        Handler handler = new Handler(mHandlerThreadSensor.getLooper());

        mSensorManager.registerListener(mListener,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                delay, handler);

        //Log.d(TAG, "Sensors have been registered at: " + frequency + "Hz (" + delay + ")");
        mLogger.fine("Sensors have been registered at: " + frequency + "Hz (" + delay + ")");

    }

    private void registerSensor() {
        mFrequencySelection = Utility.getPrefSensorFrequency(this);
        setSensorDelay(mFrequencySelection);
    }

    private void activation(double[] data) {
        double mag = Math.sqrt(data[2] * data[2] + data[3] * data[3] + data[4] * data[4]);
        double sigma;

        double mm = (mNum * mMuM + mag) / (mNum + 1);
        mSigmaSqM = (mNum * (mMuM * mMuM + mSigmaSqM) + mag * mag) / (mNum + 1) - mm * mm;
        sigma = Math.sqrt(mSigmaSqM);
        mMuM = mm;
        mNum++;

        if (!mActivityValid) {
            mActivityState = false;
            mActivityValid = true;

            return;
        }

        if (mActivityValid && !mActivityState) {
            if (mag > mMuM - mNormalRange * sigma && mag < mMuM + mNormalRange * sigma)
                return;
            else {
                // Start of activity
                //Log.d(TAG, "Entering full power ("+data[0]+")");
                mLogger.fine("Entering full power ("+data[0]+")");
                mSensorManager.unregisterListener(mListener);
                if (!mWakeLock.isHeld()) {
                    mWakeLock.acquire();
                    //Log.d(TAG, "WakeLock acquired");
                    mLogger.fine("WakeLock acquired");
                }
                setSensorDelay(50);
                mActivityState = true;
                mActivityStart = data[0];
                mActivityEnd = data[0];
            }
        } else if (mActivityValid && mActivityState) {
            if (data[0] - mActivityEnd < mActivityBreak) {
                if (mag > mMuM - mNormalRange * sigma && mag < mMuM + mNormalRange * sigma)
                    return;
                else {
                    mActivityState = true;
                    mActivityEnd = data[0];
                }
            } else {
                if (mag > mMuM - mNormalRange * sigma && mag < mMuM + mNormalRange * sigma) {
                    // Start of inactivity
                    //Log.d(TAG, "Entering low power ("+data[0]+")");
                    mLogger.fine("Entering low power ("+data[0]+")");
                    mSensorManager.unregisterListener(mListener);
                    if (mWakeLock.isHeld()) {
                        mWakeLock.release();
                        //Log.d(TAG, "WakeLock released");
                        mLogger.fine("WakeLock released");
                    }
                    setSensorDelay(10);
                    mActivityValid = false;
                    mActivityState = false;
                } else {
                    mActivityState = true;
                    mActivityStart = data[0];
                    mActivityEnd = data[0];
                }
            }
        }
    }
}
