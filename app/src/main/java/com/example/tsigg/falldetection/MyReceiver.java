package com.example.tsigg.falldetection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.tsigg.falldetectionlib.ClassifyService;
import com.example.tsigg.falldetectionlib.LibUtility;
import com.example.tsigg.falldetectionlib.classifiers.FallDetectionClassifier;
import com.example.tsigg.falldetectionlib.classifiers.FallDetectionClassifierFactory;

import java.util.Arrays;
import java.util.logging.Logger;

import weka.core.Instances;

public class MyReceiver extends BroadcastReceiver {

    public static final String TAG = MyReceiver.class.getSimpleName();
    //private static final String CSV_HEADER = "AAMV,IDI,MPI,MVI,PDI,ARI,FFI,SCI,MEAN,STD,MEDIAN,SKEW,KURT,POWER,RMS,DEVMEAN,IQR,IMPACT_POWER,IMPACT_STD,MAX_ENERGY,TOTAL_ENERGY,ENERGY_RATIO,PEAKS,VECTOR_ANGLE,VVT";
    //private static final char CSV_DELIM = ',';
    //private static final String TAG = MyReceiver.class.getSimpleName();
    //private static PrintWriter mPrintWriter;
    //private static String mPath, mFilename = "Features.csv";
    //private static MlpClassifier mClassifier1;
    //private static KnnClassifier mClassifier2;
    //private static SvmClassifier mClassifier3;
    //private static double[] mFeatures;
    private static double[] mFeatures;
    private Logger mLogger;

    public MyReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        mLogger = LibUtility.getFallDetectionLogger(context);
        //Log.d(TAG, intent.getAction() + " received");
        mLogger.fine(intent.getAction() + " received");

        if (intent.getAction().equals(LibUtility.ACTION_FSM_FALL)) {
            Bundle bundle = intent.getExtras();
            Intent classifyIntent = new Intent(context, ClassifyService.class);
            classifyIntent.putExtras(bundle);
            classifyIntent.setAction(LibUtility.ACTION_CLASSIFY);
            context.startService(classifyIntent);
            //Log.d(TAG, "Start classify intent sent");
            mLogger.fine("Start classify intent sent");
        }

        else if (intent.getAction().equals(LibUtility.ACTION_FALL)) {
            Intent alarmIntent = new Intent(context, AlarmActivity.class);
            mFeatures = intent.getDoubleArrayExtra(LibUtility.TAG_FEATURES);
            //Log.d(TAG, ""+ Arrays.toString(mFeatures));
            alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(alarmIntent);
        } else if (intent.getAction().equals(Utility.ACTION_FALSE_POSITIVE_DETECTED)) {
            //Log.d(TAG, "False Positive detected.");
            //Log.d(TAG, ""+ Arrays.toString(mFeatures));
            if (Arrays.binarySearch(mFeatures, Double.NaN) != -1) {
                int s = FallDetectionClassifierFactory.addInstance(mFeatures, LibUtility.CLASS_ADL, false);
                //Log.d(TAG, "" + s);
                FallDetectionClassifierFactory factory = new FallDetectionClassifierFactory(context);
                FallDetectionClassifier classifier = factory.getClassifier(Utility.TAG_CLASSIFIER);
                //String classifierPath = LibUtility.getExternalStorageDirectory(context) + File.separator +
                //        LibUtility.FOLDER_CLASSIFIER + File.separator + Utility.FILE_MODEL_CLASSIFIER;
                Instances data = FallDetectionClassifierFactory.getInstances(context);
                classifier.train(data, null);
                //Log.d(TAG, "Classifier trained");
            }

        } else if (intent.getAction().equals(Utility.ACTION_FALSE_NEGATIVE_DETECTED)) {
            //Log.d(TAG, "False Negative detected.");
            //Log.d(TAG, ""+ Arrays.toString(mFeatures));
            // TODO: Get classification segment
            // TODO: Get features
            // TODO: Train


        } else if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            //Log.d(TAG, "BOOT_COMPLETED");

        } else if (intent.getAction().equals("android.intent.action.BATTERY_LOW")) {
            //Log.d(TAG, "BATTERY_LOW");

        } else if (intent.getAction().equals("android.intent.action.BATTERY_OK")) {
            //Log.d(TAG, "BATTERY_OK");

        }
    }
}
